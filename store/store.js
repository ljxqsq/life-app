import {
	createStore
} from 'vuex'
import moduleUser from '@/store/user.js'
 
const store = createStore({
	modules: {
		'm_user': moduleUser
	}
})
export default store