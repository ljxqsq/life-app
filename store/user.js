export default {
	//开启命名空间
	namespaced: true,
	state: () => ({
		userinfo:JSON.parse(uni.getStorageSync('userinfo') || '{}')
	}),
	mutations:{
		//更新用户信息
		updateUserinfo(state, userinfo) {
			state.userinfo = userinfo
			this.commit('m_user/saveUserinfoStorage')
		},
		saveUserinfoStorage(state){
			uni.setStorageSync('userinfo', JSON.stringify(state.userinfo))
		}
	},
	getters:{}
}