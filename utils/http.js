import axios from 'axios';
import mpAdapter from 'axios-miniprogram-adapter'

axios.defaults.adapter = mpAdapter
// 创建axios实例
const service = axios.create({
	baseURL: 'https://api.oioweb.cn', // 你的API基地址
	timeout: 5000 // 请求超时时间
});

// 请求拦截器
service.interceptors.request.use(
	config => {
		// 可以在这里添加跨域请求需要的token等
		// if (store.getters.token) {
		//   config.headers['Authorization'] = `Bearer ${store.getters.token}`;
		// }
		return config;
	},
	error => {
		// 请求错误处理
		return Promise.reject(error);
	}
);

// 响应拦截器
service.interceptors.response.use(
	response => {
		// 对响应数据做处理，例如只返回data部分
		const res = response.data;
		// if (res.code !== 200) {
		//   // 错误处理，可以根据项目需求来
		//   Message({
		//     message: res.message || 'Error',
		//     type: 'error',
		//     duration: 5 * 1000
		//   });
		//   return Promise.reject(new Error(res.message || 'Error'));
		// }
		return res;
	},
	error => {
		// 响应错误处理
		return Promise.reject(error);
	}
);

export default service;