export default {
	dbName:'first',// 数据库名称
	dbPath: '_doc/test.db', // 数据库地址,推荐以下划线为开头   _doc/xxx.db
	//打开数据库
	openDB() {
		return new Promise((callback) => {
			plus.sqlite.openDatabase({
				name: this.dbName,
				path: this.dbPath,
				success: function(e) {
					callback({
						code: 1,
						msg: '打开数据库 成功!'
					})
				},
				fail: function(e) {
					callback({
						code: 0,
						msg: '打开数据库 失败: ' + JSON.stringify(e)
					})
				}
			});
		})
	},
	//判断数据库是否打开
	isopen() {
		return new Promise((callback) => {
			if (plus.sqlite.isOpenDatabase({
					name: this.dbName,
					path: this.dbPath,
				})) {
				callback({
					code: 1,
					msg: '已经打开数据库。'
				});
				return true;
			} else {
				callback({
					code: 0,
					msg: '没有打开数据库！'
				});
				return false;
			}
		})
	},
	//判断数据库是否打开，如果没有打开就打开，如果打开就继续执行
	OpenExist(){
		return new Promise((callback) => {
			if (plus.sqlite.isOpenDatabase({
					name: this.dbName,
					path: this.dbPath,
				})) {
				callback({
					code: 1,
					msg: '已经打开数据库'
				});
			} else {
				plus.sqlite.openDatabase({
					name: this.dbName,
					path: this.dbPath,
					success: function(e) {
						callback({
							code: 1,
							msg: '已经打开数据库'
						})
					},
					fail: function(e) {
						callback({
							code: 0,
							msg: '打开数据库 失败: ' + JSON.stringify(e)
						})
					}
				});
			}
		})
	},
	//关闭数据库
	//每次操作数据库都要关闭
	closeDB() {
		return new Promise((callback) => {
			plus.sqlite.closeDatabase({
				name: this.dbName,
				success: function(e) {
					callback({
						code: 1,
						msg: '关闭数据库成功。'
					});
				},
				fail: function(e) {
					callback({
						code: 0,
						msg: '关闭数据库失败：' + JSON.stringify(e)
					});
				}
			});
		})
	},
	//执行sql语句：对表、表中的数据进行CRUD操作
	executeSQL(sql) {
		return new Promise((callback) => {
			plus.sqlite.executeSql({
				name: this.dbName,
				sql: sql,
				success: function(e) {
					callback({
						code: 1,
						msg: '执行SQL成功'
					});
				},
				fail: function(e) {
					callback({
						code: 0,
						msg: '执行SQL失败：' + JSON.stringify(e)
					});
				}
			});
		})
	},
	//查询sql
	selectSQL(sql) {
		return new Promise((callback) => {
			plus.sqlite.selectSql({
				name: this.dbName,
				sql: sql,
				success: function(data) {
					callback({
						code: 1,
						data: data,
						msg: '查询SQL成功'
					})
					// for (var i in data) {
					// 	uni.$showMsg('查询sql数据: 第' + i + '条数据' + JSON.stringify(data[i]))
					// }
				},
				fail: function(e) {
					callback({
						code: 0,
						data: [],
						msg: '查询SQL失败: ' + JSON.stringify(e)
					})
				}
			});
		})
	},
	//开始事务  'begin'  'commit'  'rollback'
	transaction(oper) {
		return new Promise((callback) => {
			plus.sqlite.transaction({
				name: this.dbName,
				operation: oper,
				success: function(e) {
					callback({
						code: 1,
						msg: '事务 ' + oper + ' 成功'
					})
				},
				fail: function(e) {
					callback({
						code: 0,
						msg: '事务 ' + oper + ' 失败：' + JSON.stringify(e)
					})
				}
			});
		})
	}
}