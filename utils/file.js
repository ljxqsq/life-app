export function createFlie(path, data) {
	return new Promise(resolve => { //这里封装了个是一个promise异步请求
		plus.io.requestFileSystem(
			plus.io.PUBLIC_DOWNLOADS, // 文件系统中的根目录下的DOCUMENTS目录
			fs => {
				// 创建或打开文件, fs.root是根目录操作对象,直接fs表示当前操作对象
				fs.root.getFile(path, {
					create: true // 文件不存在则创建
				}, fileEntry => {
					// 文件在手机中的路径
					// console.log(fileEntry.fullPath)
					fileEntry.createWriter(writer => {
						// 写入文件成功完成的回调函数
						writer.onwrite = e => {
							console.log("写入本地文件成功");
							resolve("写入本地文件")
						};
						// 写入数据
						writer.write(JSON.stringify(data));
					})
				}, e => {
					console.log("getFile failed: " + e.message);
				});
			},
			e => {
				console.log(e.message);
			}
		);
	})
}
//查看我们新建的documents目录下的所有文件,或者删除指定文件
export function getFileList(path) {
	plus.io.requestFileSystem(plus.io.PRIVATE_WWW, function(fs) {
		// fs.root是根目录操作对象DirectoryEntry
		// 创建读取目录信息对象 
		var directoryReader = fs.root.createReader();
		directoryReader.readEntries(function(entries) {
			var i;
			for (i = 0; i < entries.length; i++) {
				console.log('文件', entries[i].name);
			}
		}, function(e) {
			alert("Read entries failed: " + e.message);
		});
	});

}
//查看目录下的某个指定文件
export function getFileData(path) {
	return new Promise(resolve => { //文件读写是一个异步请求 用promise包起来方便使用时的async+await
		plus.io.requestFileSystem(
			plus.io.PRIVATE_WWW,
			fs => {
				fs.root.getFile(path, {
					create: false
				}, fileEntry => {
					fileEntry.file((file) => {
						console.log("文件大小:" + file.size + '-- 文件名:' + file.name);
						//创建读取文件对象
						let fileReader = new plus.io.FileReader();
						//以文本格式读取文件数据内容
						fileReader.readAsText(file, 'utf-8');
						//文件读取操作完成时的回调函数
						fileReader.onloadend = (evt) => {
							console.log(JSON.parse(evt.target.result),
								'JSON.parse(evt.target.result)')
							resolve(JSON.parse(evt.target.result))
							// sURL = JSON.parse(evt.target.result).URL;

						}
					});
				}, e => {
					console.log("getFile failed: " + e.message);
				});
			},
			e => {
				console.log(e.message);
			}
		);
	})
}